// @flow
import React, {Component} from "react";
import {Dimensions} from "react-native";
import {StyleProvider} from "native-base";
import {StackNavigator, DrawerNavigator} from "react-navigation";
import {Font, AppLoading} from "expo";
import {useStrict} from "mobx";

import {Images} from "./src/components";
import {Login} from "./src/login";
import {SignUp} from "./src/sign-up";
import {ForgotPassword} from "./src/forgot-password";
import {Drawer} from "./src/drawer";
import {Top} from "./src/top";
import {Duel} from "./src/duel";
import {Overview} from "./src/overview";
import {Activity} from "./src/activity";
import {Lists} from "./src/lists";
import {Profile} from "./src/profile";
import {Timeline} from "./src/timeline";
import {Settings} from "./src/settings";
import {Create} from "./src/create";
import {Topdetails} from "./src/topdetails";
import {Wallet} from "./src/wallet";
import {Adddual} from "./src/adddual";
import {Viewdualprofile} from "./src/viewdualprofile";
import {Adddualprofile} from "./src/adddualprofile";
import {Quiz} from "./src/quiz";
import {Quizlist} from "./src/quizlist";
import {Addquiz} from "./src/addquiz";
import {Addtop} from "./src/addtop";

import getTheme from "./native-base-theme/components";
import variables from "./native-base-theme/variables/commonColor";

export default class App extends Component {

    state: { ready: boolean } = {
        ready: false,
    };

    componentWillMount() {
        const promises = [];
        promises.push(
            Font.loadAsync({
                "Avenir-Book": require("./fonts/Avenir-Book.ttf"),
                "Avenir-Light": require("./fonts/Avenir-Light.ttf"),
                "Ionicons":require("./fonts/Ionicons.ttf")
            })
        );
        Promise.all(promises.concat(Images.downloadAsync()))
            .then(() => this.setState({ ready: true }))
            // eslint-disable-next-line
            .catch(error => console.error(error))
        ;
    }

    render(): React$Element<*> {
        const {ready} = this.state;
        return <StyleProvider style={getTheme(variables)}>
            {
                ready ?
                    <AppNavigator onNavigationStateChange={() => undefined} />
                :
                    <AppLoading />
            }
        </StyleProvider>;
    }
}

useStrict(true);

const LeftNavigator = DrawerNavigator({
    Top: { screen: Top },
    Dual: { screen: Duel },
    QuizList: { screen: Quizlist },
    Activity: { screen: Activity },
    RankList: { screen: Lists }
}, {
    drawerWidth: Dimensions.get("window").width,
    contentComponent: Drawer,
    drawerPosition:'left'
});



const AppNavigator = StackNavigator({
    Login: { screen: Login },
    SignUp: { screen: SignUp },
    ForgotPassword: { screen: ForgotPassword },
    Main: { screen: LeftNavigator },
    Topdetails: { screen: Topdetails },
    Adddual: { screen: Adddual },
    Viewdualprofile:{ screen: Viewdualprofile },
    Adddualprofile: { screen: Adddualprofile },
    Dual: { screen: Duel },
    Quiz: { screen: Quiz },
    Addquiz: { screen: Addquiz },
    Profile:{ screen: Profile },
    Wallet:{ screen: Wallet },
    Addtop:{ screen: Addtop }
}, {
    headerMode: "none"
});

export {AppNavigator};
