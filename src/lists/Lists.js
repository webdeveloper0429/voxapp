// @flow
import autobind from "autobind-decorator";
import React, {Component} from "react";
import {StyleSheet, Image, View, Text, TouchableOpacity,ScrollView, Footer,ActivityIndicator} from "react-native";
import {H1,H3, Button, Icon} from "native-base";
import {observable, action} from "mobx";
import { observer } from "mobx-react/native";
import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";

import {BaseContainer, Styles, Images, Avatar,WindowDimensions} from "../components";

import variables from "../../native-base-theme/variables/commonColor";
import Small from "../components/Small";
import api from '../Utils/api';
import helper from '../Utils/helper';

export default class Lists extends Component {
    

    constructor(props) {

        super(props)

        this.state = {
          loading: true,
          rankList:[],
          currentuserid:'',
        }
       
    }
    getData(user_id){

            let data ={
                user_id:user_id
            }
            api.postApi('rank_list_service.php',data)
                  .then((res) => {

                    
                    if(res.Result=='True'){
                     // console.log(res.data);
                        this.setState({loading:false,rankList:res.data,currentuserid:user_id}); 
                    }else{

                         this.setState({loading:false}); 
                    }
                   
            })
            .catch((e) => {

                     //alert(e.message);
                //this.refs.toast.show(e.message);
                this.setState({loading:false});

            })
    }

    componentWillMount() {

        helper.getCache('@userdata').then((res) => {

           
            if(res){

                this.userData = JSON.parse(res);

                this.getData(this.userData.user_id);
                
            }else{

                this.setState({loading:false}); 
            }
            
           
        })

        
    //const userdata = await AsyncStorage.getItem('@userdata'); 
    }


    go(key: string) {
        this.props.navigation.navigate(key);
    }

    render(): React$Element<*> {
        const {rankList, loading,currentuserid} = this.state;
        if (loading) {

            return <BaseContainer title="Rank List" navigation={this.props.navigation} scrollable>
                                    <Image source={Images.drawer} style={styles.img} >
            <ScrollView style={styles.imgMask }>
            {
                    <View style={ styles.flex }>
                      <ActivityIndicator
                        animating={ loading }
                        style={[
                          styles.centering,
                          { height: 80 }
                        ]} size="large" />
                    </View>
                }
                                                </ScrollView>
                </Image>
                </BaseContainer>;
            

        }else{

            return <BaseContainer title="Rank List" navigation={this.props.navigation} scrollable>
                                    <Image source={Images.drawer} style={styles.img} >
            <ScrollView style={styles.imgMask }>
            {
                !loading && <View>

                    {
                        rankList.length==0 && <View>
                             <Text style={{flex:1, flexDirection:'row',fontSize:20,padding:20,
         textAlign:'center'}}>No Records Found</Text>
                        </View>
                    }
                    {
                        _.map(
                            rankList,
                            (item, key) => <Item
                                key={key}
                                selectkey={key}
                                navigation={this.props.navigation}
                                name={item.username}
                                rank={key}
                                photo={item.photo}
                                currentuserid={currentuserid}
                                userid={item.user_id}
                            />
                        )
                    }
       
                </View>
            }
                                            </ScrollView>
                </Image>
            </BaseContainer>;
        }
        
        
    }
}


@observer
class Item extends Component {

    props: {
        name: string,
        rank:string,
        userid:number,
        currentuserid:number,
        navigation:any
        
    }


    @autobind @action
    toggle() {
         const {userid,navigation,name,selectkey,currentuserid} = this.props;
         navigation.navigate("Topdetails",{name:name,userid:userid,currentuserid:currentuserid,key:selectkey});
    }

    render(): React$Element<*>  {
        const {name,rank,userid,currentuserid,photo} = this.props;
        const btnStyle ={ backgroundColor: this.done ? variables.brandInfo : variables.lightGray };
        return <View style={Styles.listItem}>

            {currentuserid!=userid&&<TouchableOpacity style={[Styles.center, styles.title, styles.topnames]}  onPress={this.toggle}>
                
                
            <View style={styles.toplist}>   
                <View style={styles.lefttop}>
                    <H3 style={styles.top_name}>{name}</H3>
                    <Text style={styles.gray}>Rank - {rank+1}</Text>
                </View>
                <View style={styles.middletop}>
                     {photo!=''&&<Image style={{width:50,height:50}} source={{uri:photo}}/>}
                {photo==''&&<Avatar size={50}/>}
                </View>
                <View>
                    <H3 style={styles.top_vp}>100 VP</H3>
                </View>
            </View>
            </TouchableOpacity>}

             {currentuserid==userid&&<TouchableOpacity style={[Styles.center, styles.title, styles.topnames, styles.activename]}  onPress={this.toggle}>
                
               <View style={[styles.toplist, styles.activecolor]}>   
                    <View style={styles.lefttop}>
                        <H3 style={styles.top_name}>{name}</H3>
                        <Text style={styles.gray}>Rank - {rank+1}</Text>
                    </View>
                    <View style={styles.middletop}>
                         {photo!=''&&<Image style={{width:50,height:50}} source={{uri:photo}}/>}
                {photo==''&&<Avatar size={50}/>}
                    </View>
                    <View>
                        <H3 style={styles.top_vp}>0 VP</H3>
                    </View>
                </View>
            </TouchableOpacity>}

        </View>;

    }
}


const styles = StyleSheet.create({
    mask: {
        backgroundColor: "rgba(0, 0, 0, .5)"
    },
    button: {
        height: 75, width: 75, borderRadius: 0
    },
    title: {
        paddingLeft: variables.contentPadding,
        padding:20
    },
    userrank:{
        flex:2, 
        left: 0, 
        right: 0, 
        bottom: 0
    },
    namebold:{
        fontWeight:'bold',
                color:'#00CE9F'
    },
    topnames:{
        flex:1,
        flexDirection:'row',
        justifyContent: 'space-between',
    },
    name:{
        fontSize:16
    },
    activename:{
        backgroundColor:'#999'
    },
     title: {
        justifyContent: "center",
        flex: 1,
        padding: variables.contentPadding
    },
        gray: {
        color: variables.gray
    },
    toplist:{
        flex:1,
        flexDirection:'row',
        justifyContent: 'space-between',
    },
    top_name:{
        lineHeight:20,
        color:'#635DB7'
    },

    lefttop:{
        width:130
    },
    middletop:{
        width:40
    },
    top_vp:{
        color:'#00CE9F'
    },
            img: {
        resizeMode: "cover",
        ...WindowDimensions
    },
            imgMask: {
        backgroundColor: "rgba(71, 255, 247, .4)"
    }
});