// @flow
import autobind from "autobind-decorator";
import React, {Component} from "react";
import {View, Image, StyleSheet, KeyboardAvoidingView, ScrollView , TouchableOpacity,Text, Picker,ActivityIndicator} from "react-native";
import {H1} from "native-base";
import {Container, Button, Header, Left, Right, Body, Icon, Title} from "native-base";
import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";
import { EvilIcons } from "@expo/vector-icons";
import { Col, Row, Grid } from 'react-native-easy-grid';
import {BaseContainer, Avatar, Images, TaskOverview, Small, Styles, Task, Field, NavigationHelpers} from "../components";

import variables from "../../native-base-theme/variables/commonColor";

import Toast,{DURATION} from "react-native-easy-toast";

import api from '../Utils/api';
import helper from '../Utils/helper';

export default class Adddualprofile extends Component {

    constructor(props) {
        
        super(props)
        this.state = {
          loading: true,
          selectuserkey:this.props.navigation.state.params.key,
          name:this.props.navigation.state.params.name,
          userid:this.props.navigation.state.params.userid,
          currentuserid:this.props.navigation.state.params.currentuserid
        }

       console.log('key====',this.props.navigation.state.params.key);
    }

    props: {
        navigation: NavigationScreenProp<*, *>
    }

    componentWillMount() {

        
        this.getData();
    //const userdata = await AsyncStorage.getItem('@userdata'); 
    }

    @autobind
    back() {

        this.props.navigation.goBack();
    }

    @autobind
    signIn() {

        NavigationHelpers.reset(this.props.navigation, "Walkthrough");
    }

    getData(){

            let data ={
                user_id:this.state.currentuserid
            }

            api.postApi('top10_list_service.php',data)
                  .then((res) => {

                    if(res.Result=='True'){
                        
                        this.userList = res.data;
                        console.log(res.data[this.state.selectuserkey]);
                        this.setState({loading:false,jsonData:res.data[this.state.selectuserkey]}); 

                    }else{

                         this.setState({loading:false}); 
                    }
                   
            })
            .catch((e) => {

                     //alert(e.message);
                //this.refs.toast.show(e.message);
                this.setState({loading:false});

            })
    }

    nextAction = (type) => {

        

        if(type=='next'&&this.userList.length>(this.state.selectuserkey+1)){

           
            this.state.selectuserkey = this.state.selectuserkey+1;

        }else if(type=='prev'&&this.state.selectuserkey>0){

            this.state.selectuserkey = this.state.selectuserkey-1;
        }

        console.log('this.state.selectuserkey==',this.state.selectuserkey);

        this.setState({jsonData:this.userList[this.state.selectuserkey]}); 
    }

    ratingAction = (number) => {

        

            let data ={
                user_id:this.state.currentuserid,
                rating:number
            }

            console.log(data);

            api.postApi('user_rating_service.php',data)
                  .then((res) => {

                    console.log(res);
                    if(res.Result=='True'){
                        
                        this.refs.toast.show(res.ResponseMsg);
                        //this.setState({loading:false,jsonData:res.data[this.state.selectuserkey]}); 

                    }else{

                        this.refs.toast.show(res.ResponseMsg);
                        // this.setState({loading:false}); 
                    }
                   
            })
            .catch((e) => {

                     //alert(e.message);
                console.log(e);
                this.refs.toast.show(e.message);
                this.setState({loading:false});

            })
    }

    render(): React$Element<*> {

        const {name, loading} = this.state;
        
        if (loading) {

            return <Container>
                <Header noShadow>
                    <Left>
                        <Button onPress={this.back} transparent>
                            <Icon name='close' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>{name}</Title>
                    </Body>
                    <Right />
                </Header>
                <ScrollView style={{ backgroundColor: "white", flex: 1 }} >
                    
                     <View style={ Styles.flex }>
                      <ActivityIndicator
                        animating={ loading }
                        style={[
                          Styles.centering,
                          { height: 80 }
                        ]} size="large" />
                    </View>

                </ScrollView>

            </Container>;

        }else{

            return <Container>
                <Header noShadow>
                    <Left>
                        <Button onPress={this.back} transparent>
                            <Icon name='close' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>{this.state.jsonData.top_name}</Title>
                    </Body>
                    <Right />
                </Header>
                <ScrollView style={{ backgroundColor: "white", flex: 1 }} >
                  <View style={{flex: 1, flexDirection: 'row',  justifyContent: 'space-between'}}>
                    <View style={{ margin:5}} >
                        <Text style={{ fontSize:25}}>VP: {this.state.jsonData.vp_token}</Text>
                    </View>
                    <View style={{ margin:5}} >
                        <Text style={{ fontSize:25}}>Ratings: 0</Text>
                    </View>
                  </View>
                 <Image source={Images.timeline} style={Styles.header}>
                    <View style={[Styles.imgMask,style.date ]}>
                    <TouchableOpacity style={{width: 50, paddingTop:90,  alignItems: 'center'}} onPress={() => this.nextAction('prev')}>
                        <EvilIcons name="arrow-left" size={40} color={variables.white} />
                    </TouchableOpacity> 
                    <View style={{width: 150, paddingTop:20, flex: 1, alignItems: 'center'}} >
                        
                        {this.state.jsonData.top_image!=''&&<Image style={{width:170,height:170}} source={{uri:this.state.jsonData.top_image}}/>}
                        {this.state.jsonData.top_image==''&&<Avatar size={170}/>}

                    </View>
                    <TouchableOpacity style={{width: 50,  paddingTop:90, alignItems: 'center'}} onPress={() => this.nextAction('next')}>
                        <EvilIcons name="arrow-right" size={40} color={variables.white} />
                    </TouchableOpacity> 
                    </View>
                </Image>
                <View style={{ flex:1, flexDirection:'row', backgroundColor:'#fff', justifyContent: 'center'}}>
                    <TouchableOpacity style={style.rating} >
                        <Button primary onPress={() => this.ratingAction('+5')} style={style.ratinginner} ><Text style={style.ratingtext}>05</Text></Button>
                    </TouchableOpacity> 
                    <TouchableOpacity style={style.rating} >
                        <Button primary onPress={() => this.ratingAction('+10')} style={style.ratinginner}><Text style={style.ratingtext}>10</Text></Button>
                    </TouchableOpacity> 
                    <TouchableOpacity style={style.rating} >
                        <Button primary onPress={() => this.ratingAction('-5')} style={style.ratinginner}><Text style={style.ratingtext}>-5</Text></Button>
                    </TouchableOpacity> 
                    <TouchableOpacity style={style.rating} >
                        <Button primary onPress={() => this.ratingAction('-10')} style={style.ratinginner}><Text style={style.ratingtext}>-10</Text></Button>
                    </TouchableOpacity> 
                </View>   
                </ScrollView>
                 <Toast ref="toast"/> 
            </Container>;
        }
        
    }
}

const style = StyleSheet.create({
    heading: {
        marginTop: variables.contentPadding * 2,
        color: "white"
    },
    date:{
    flexDirection: 'row',
    height:550
  },
  rating:{
    margin:12
  },
ratinginner:{
    alignItems: 'center', 
    justifyContent: 'center',
    width:60, 
    height:60, 
    padding:20,
    borderRadius: 100
  },
  ratingtext:{
    color:'#fff',
    fontSize:16
  }
});