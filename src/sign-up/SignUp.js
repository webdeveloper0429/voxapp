// @flow
import autobind from "autobind-decorator";
import React, {Component} from "react";
import {View, Image, StyleSheet, KeyboardAvoidingView, ScrollView, TouchableOpacity} from "react-native";
import {Container, Button, Header, Left, Right, Body, Icon, Title,Spinner} from "native-base";
import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";

import {Styles, Images, Field, NavigationHelpers, Avatar} from "../components";

import variables from "../../native-base-theme/variables/commonColor";

import api from '../Utils/api';
import helper from '../Utils/helper';

import Toast,{DURATION} from "react-native-easy-toast";
import { ImagePicker } from 'expo';

export default class SignUp extends Component {

    props: {
        navigation: NavigationScreenProp<*, *>
    }

    constructor(props) {
        super(props)

        this.state = {
            isLoading: false,
            user_name:'',
            photo:'',
            user_email:'',
            user_password:'',
            user_phonenumber:'',
            user_age:'',
            user_country:'',
            city_town:'',
            user_repeatpassword:''

        }

       
  }

    @autobind
    back() {
        this.props.navigation.goBack();
    }

     @autobind
    async getImage(): Promise<void> {
        console.log('camera start');
        // CameraRoll.getPhotos({
        //     first:20
        // }).then(r=>{
        //     console.log(r.edges);
        // }).catch((err)=>{

        //     console.log('err images===',err);
        // })

        let result = await ImagePicker.launchCameraAsync({
            allowEditing:true,
            aspect:[4,3]
        })

        //console.log(result);

        if(!result.cancelled){

            console.log(result.uri);
            this.imagechange =true;
            this.setState({photo:result.uri,disabled:false});
        }
    }

    @autobind
    signIn() {
        try {
            
             var user_email = this.state.user_email;
             var user_name = this.state.user_name;
             var user_password = this.state.user_password;
             var user_phonenumber = this.state.user_phonenumber;
             var user_age = this.state.user_age;
             var user_country = this.state.user_country;
             var city_town = this.state.city_town;
             var user_repeatpassword = this.state.user_repeatpassword;

             var emailRegex = /^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$/;

             if(user_name==''){

                this.refs.toast.show('Please provide username');

             }else if(user_email==''){

                this.refs.toast.show('Please provide email address');

             }else if(!emailRegex.test(user_email)){

                this.refs.toast.show('Email format is invalid');

             }else if(user_password==''){

                this.refs.toast.show('Please provide password');

             }else if(user_password!=user_repeatpassword){

                this.refs.toast.show('Password mismatch');
             }else{

                this.setState({isLoading:true});
                let photo ='';

                if(this.state.photo!=''){

                    photo ={
                        uri:this.state.photo,
                        name:'profile.jpg',
                        type:'image/jpg'
                    } 
                }

                let data ={
                    user_name:user_name,
                    user_email:user_email,
                    user_password:user_password,
                    user_phonenumber:user_phonenumber,
                    user_age:user_age,
                    user_country:user_country,
                    city_town:city_town,
                    photo:photo
                }

                console.log(data);

                api.postApi('user_register_service.php',data)
                  .then((res) => {

                    //console.log(res);
                    this.setState({isLoading:false});

                    if(res.Result=='True'){

                        helper.setCache('@userdata',res.data);
                        NavigationHelpers.reset(this.props.navigation, "Main");

                    }else{

                        this.refs.toast.show(res.ResponseMsg);
                    }

                    
                    
                  })
                  .catch((e) => {
                     //alert(e.message);
                     this.refs.toast.show(e.message);
                     this.setState({isLoading:false});
                  })


             }

             

        } catch(e) {
            this.setState({isLoading:false});
            this.refs.toast.show(e.message);
        } 
    }

    render(): React$Element<*> {
        return <Container>

            <Header noShadow>
                <Left>
                    <Button onPress={this.back} transparent>
                        <Icon name='close' />
                    </Button>
                </Left>
                <Body>
                    <Title>Registeration</Title>
                </Body>
                <Right />
            </Header>
            <Toast ref="toast"/> 
            <ScrollView style={{ backgroundColor: "white", flex: 1 }} >
                <KeyboardAvoidingView behavior="padding">
                    <View style={{flex:1, flexDirection:'row', justifyContent:'center' }}>
                         <TouchableOpacity onPress={this.getImage}>
                         {this.state.photo!=''&&<Image style={{width:100,height:100}} source={{uri:this.state.photo}}/>}
                        {this.state.photo==''&&<Avatar size={100}/>}
                        </TouchableOpacity>
                        </View>
                    <Field label="Username" autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="default"
                                returnKeyType="next"
                                value={this.state.user_name}
                                onChangeText={(user_name) => this.setState({user_name})}
                                 />
                    <Field label="Email"
                                autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="email-address"
                                returnKeyType="next"
                                value={this.state.user_email}
                                onChangeText={(user_email) => this.setState({user_email})}
                                 />
                    <Field label="Phone Number" 
                                autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="phone-pad"
                                returnKeyType="next"
                                value={this.state.user_phonenumber}
                                onChangeText={(user_phonenumber) => this.setState({user_phonenumber})}
                                />
                    <Field label="Age" 
                                autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="phone-pad"
                                returnKeyType="next"
                                value={this.state.user_age}
                                onChangeText={(user_age) => this.setState({user_age})}
                                />
                    <Field label="Country" 
                                autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="default"
                                returnKeyType="next"
                                value={this.state.user_country}
                                onChangeText={(user_country) => this.setState({user_country})}
                                />
                    <Field label="City/Town" 
                                autoCapitalize="none"
                                autoCorrect={false}tger
                                keyboardType="default"
                                returnKeyType="next"
                                value={this.state.city_town}
                                onChangeText={(city_town) => this.setState({city_town})}
                                />
                    <Field label="Password" secureTextEntry 
                                autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="default"
                                returnKeyType="next"
                                value={this.state.user_password}
                               onChangeText={(user_password) => this.setState({user_password})}  />
                    <Field label="Repeat Password" secureTextEntry secureTextEntry 
                                autoCapitalize="none"
                                autoCorrect={false} 
                                keyboardType="default"
                                returnKeyType="next"
                                value={this.state.user_repeatpassword}
                                onChangeText={(user_repeatpassword) => this.setState({user_repeatpassword})}  />
                </KeyboardAvoidingView>
            </ScrollView>
            <Button primary full onPress={this.signIn} style={{height: variables.footerHeight}}>
                {this.state.isLoading ? <Spinner color="white"/> : <Icon name="md-checkmark"/>}
            </Button>
        </Container>;
    }
}

const style = StyleSheet.create({
    circle: {
        backgroundColor: "white",
        height: 125,
        width: 125,
        borderRadius: 62.5,
        justifyContent: "center",
        alignItems: "center"
    }
});