// @flow
import autobind from "autobind-decorator";
import React, {Component} from "react";
import {StyleSheet, Image, View, Text, TouchableOpacity, Footer,ActivityIndicator, KeyboardAvoidingView, ScrollView} from "react-native";
import {H1,H3, Button, Icon, Container, Header, Left, Right, Body, Title,Spinner} from "native-base";
import {observable, action} from "mobx";
import { observer } from "mobx-react/native";
import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";

import {BaseContainer, Styles, Images ,Avatar, Field,NavigationHelpers, WindowDimensions} from "../components";

import variables from "../../native-base-theme/variables/commonColor";
import Small from "../components/Small";
import api from '../Utils/api';
import helper from '../Utils/helper';

import Toast,{DURATION} from "react-native-easy-toast";

import { NavigationActions } from 'react-navigation'

export default class Quizlist extends Component {
   
   constructor(props) {
    super(props)

    this.state = {
      loading: true,
      saveLoading:false,
      question:'',
      answer_a:'',
      answer_b:'',
      answer_c:'',
      answer_d:'',
      currentuserid:'',
    }

       
  }


    componentWillMount() {

        helper.getCache('@userdata').then((res) => {

            console.log(res);
            if(res){

                this.userData = JSON.parse(res);
                this.setState({loading:false}); 
                
                
            }else{

                this.setState({loading:false}); 
            }
            
           
        })

        
    //const userdata = await AsyncStorage.getItem('@userdata'); 
    }

    @autobind
    back() {
        this.props.navigation.goBack();
    }

    @autobind
    addQuiz() {
        try {
             

             

             var question = this.state.question;
             var answer_a = this.state.answer_a;
             var answer_b = this.state.answer_b;
             var answer_c = this.state.answer_c;
             var answer_d = this.state.answer_d;

             var emailRegex = /^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$/;

             if(question==''){

                this.refs.toast.show('Please provide question');

             }else if(answer_a==''){

                this.refs.toast.show('Please provide answer_a');

             }else if(answer_b==''){

                this.refs.toast.show('Please provide answer_b');

             }else if(answer_c==''){

                this.refs.toast.show('Please provide answer_c');

             }else if(answer_d==''){

                this.refs.toast.show('Please provide answer_d');

             }else{

                this.setState({saveLoading:true});
                
                let data;

                data ={
                        user_id:this.userData.user_id,
                        question:question,
                        answer_a:answer_a,
                        answer_b:answer_b,
                        answer_c:answer_c,
                        answer_d:answer_d,

                    }
                

                //console.log(data);
                api.postApi('add_quiz_service.php',data)
                  .then((res) => {

                    console.log(res);
                    this.setState({saveLoading:false});

                    if(res.Result=='True'){

                        
                        
                         const { navigation } = this.props;
                        navigation.goBack();
                        navigation.state.params.onSelect({ selected: true });

                    }else{

                     this.refs.toast.show(res.ResponseMsg);
                       
                        // NavigationHelpers.reset(this.props.navigation, "QuizList");
                    }

                    
                    
                  })
                  .catch((e) => {
                    // alert(e.message);
                     this.refs.toast.show(e.message);
                     this.setState({isLoading:false});
                  })


             }

             

        } catch(e) {
            this.setState({isLoading:false});
            this.refs.toast.show(e.message);
        } 
    }

    go(key: string) {
        this.props.navigation.navigate(key);
    }

    render(): React$Element<*> {
        const {top10, loading,currentuserid} = this.state;
        if (loading) {

            return <Image source={Images.login} style={styles.img} ><Container >
            <ScrollView style={styles.imgMask} >
                        <Header noShadow>
                <Left>
                    <Button onPress={this.back} transparent>
                        <Icon name='close' />
                    </Button>
                </Left>
                <Body>
                    <Title>Add Quiz</Title>
                </Body>
                <Right />
            </Header>
            {
                    <View style={ styles.flex }>
                      <ActivityIndicator
                        animating={ loading }
                        style={[
                          styles.centering,
                          { height: 80 }
                        ]} size="large" />
                    </View>
                }
                </ScrollView>
                </Container></Image>;
            

        }else{

            return <Image source={Images.login} style={styles.img} ><Container>
            <Toast ref="toast"/> 
            <Header noShadow>
                <Left>
                    <Button onPress={this.back} transparent>
                        <Icon name='close' />
                    </Button>
                </Left>
                <Body>
                    <Title>Add Quiz</Title>
                </Body>
                <Right />
            </Header>
            {
                <ScrollView style={styles.imgMask} >
                    <KeyboardAvoidingView behavior="padding">
                        <View style={styles.blur} >
                            <Field label="Question" 
                            autoCorrect={false}
                                keyboardType="default"
                                returnKeyType="next"
                                value={this.state.question}
                                onChangeText={(question) => this.setState({question})}/>
                            <Field label="Correct Answer"
                                autoCorrect={false}
                                keyboardType="default"
                                returnKeyType="next"
                                value={this.state.answer_a}
                                onChangeText={(answer_a) => this.setState({answer_a})}/>
                            <Field label="Wrong Answer" 
                                autoCorrect={false}
                                keyboardType="default"
                                returnKeyType="next"
                                value={this.state.answer_b}
                                onChangeText={(answer_b) => this.setState({answer_b})}/>
                            <Field label="Wrong Answer" 
                                autoCorrect={false}
                                keyboardType="default"
                                returnKeyType="next"
                                value={this.state.answer_c}
                                onChangeText={(answer_c) => this.setState({answer_c})}/>
                            <Field label="Wrong Answer" 
                                autoCorrect={false}
                                keyboardType="default"
                                returnKeyType="next"
                                value={this.state.answer_d}
                                onChangeText={(answer_d) => this.setState({answer_d})}/>
                        </View>

                    </KeyboardAvoidingView>

                    </ScrollView>

            }
                        <Button primary full onPress={this.addQuiz}>
                           {this.state.saveLoading ? <Spinner color="white"/> : <Text style={{ color:'#fff' }}>SAVE</Text>} 
                        </Button>
            </Container></Image>;
        }
        
        
    }
}




const styles = StyleSheet.create({
    mask: {
        backgroundColor: "rgba(0, 0, 0, .5)"
    },
    button: {
        height: 75, width: 75, borderRadius: 0
    },
    title: {
        paddingLeft: variables.contentPadding,
        padding:20
    },
    userrank:{
        flex:2, 
        left: 0, 
        right: 0, 
        bottom: 0
    },
    namebold:{
        fontWeight:'bold',
                color:'#00CE9F'
    },
    namebold:{
        fontWeight:'bold',
                color:'#00CE9F'
    },
    topnames:{
        flex:1,
        flexDirection:'row',
        justifyContent: 'space-between',
    },
    name:{
        fontSize:16
    },
    activename:{
        backgroundColor:'#635DB7'
    },
    activecolor:{
        color:'#fff'
    },
    title: {
        justifyContent: "center",
        flex: 1,
        padding: variables.contentPadding
    },
        gray: {
        color: variables.gray
    },
    toplist:{
        flex:1,
        flexDirection:'row',
        justifyContent: 'space-between',
    },
    top_name:{
        lineHeight:20,
        color:'#635DB7'
    },

    lefttop:{
        width:130
    },
    middletop:{
        width:40
    },
    top_vp:{
        color:'#00CE9F'
    },
        insert:{
      color:'#fff',
      fontSize:25
    },
        img: {
        resizeMode: "cover",
        ...WindowDimensions
    },
            imgMask: {
        backgroundColor: "rgba(71, 255, 247, .4)"
    },


});