// @flow
import autobind from "autobind-decorator";
import React, {Component} from "react";
import {StyleSheet, Image, View, Text, TouchableOpacity, Footer,ActivityIndicator, ScrollView} from "react-native";
import {H1, Button, Icon} from "native-base";
import {observable, action} from "mobx";
import { observer } from "mobx-react/native";
import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";

import {BaseContainer, Styles, Images,WindowDimensions} from "../components";

import variables from "../../native-base-theme/variables/commonColor";
import Small from "../components/Small";
import api from '../Utils/api';
import helper from '../Utils/helper';

export default class Calendar extends Component {
    
    constructor(props) {
    super(props)

    this.state = {
      loading: true,
      duelList:[],
    }

       
  }


    componentWillMount() {

        helper.getCache('@userdata').then((res) => {

            console.log(res);
            if(res){

                this.userData = JSON.parse(res);

                this.getData(this.userData.user_id);
                
            }else{

                this.setState({loading:false}); 
            }
            
           
        })

        
    //const userdata = await AsyncStorage.getItem('@userdata'); 
    }

    onSelect = data => {
        console.log('selctstateasss');
        this.setState({loading:true});
        this.getData(this.userData.user_id);
   };


    getData(user_id){

            let data ={
                user_id:user_id
            }
            api.postApi('duel_list_service.php',data)
                  .then((res) => {

                    
                    if(res.Result=='True'){
                       // console.log(res.data);
                        this.setState({loading:false,duelList:res.data}); 
                    }else{

                         this.setState({loading:false}); 
                    }
                   
            })
            .catch((e) => {

                     //alert(e.message);
                //this.refs.toast.show(e.message);
                this.setState({loading:false});

            })
    }


     @autobind
    go() {
        this.props.navigation.navigate('Adddual', { onSelect: this.onSelect });
    }

    render(): React$Element<*> {
        const {duelList, loading} = this.state;
        if (loading) {

            return <BaseContainer title="Dual" navigation={this.props.navigation} scrollable>
                        
            <ScrollView >
            {
                    <View style={ Styles.flexGrow }>
                      <ActivityIndicator
                        animating={ loading }
                        style={[
                          Styles.centering,
                          { height: 80 }
                        ]} size="large" />
                    </View>
                }
                                </ScrollView>
                
                </BaseContainer>;
            

        }else{

            return <Image source={Images.login} style={style.img} ><BaseContainer title="Dual" navigation={this.props.navigation} >
                        
            <ScrollView style={style.imgMask }>

            {
                !loading && <View>

                    {
                        duelList.length==0 && <View>
                             <Text style={{flex:1, flexDirection:'row',fontSize:20,padding:20,
         textAlign:'center'}}>No Records Found</Text>
                        </View>
                    }
                    {
                        _.map(
                            duelList,
                            (item, key) => <Item
                                key={key}
                                navigation={this.props.navigation}
                                firstname={item.firstname}
                                secondname={item.secondname}
                                firstvotes={item.firstvotes}
                                secondvotes={item.secondvotes}
                                rank={item.user_rank}
                                firstphoto={item.firstphoto}
                                secondphoto={item.secondphoto}
                                duel_id={item.duel_id}
                                userid={item.user_id}
                            />
                        )
                    }
                
               
                </View>
            }


                            </ScrollView>
                

              <Button primary full onPress={this.go}>
                   <Text style={style.insert}><Icon name="md-add"/> Insert</Text>
              </Button>
              
            </BaseContainer></Image>;
        }
        
        
    }
}


@observer
class Item extends Component {

    props: {
        firstname: string,
        secondname: string,
        firstvotes: number,
        secondvotes: number,
        navigation:any,
        secondphoto:string,
        duel_id:number,
        firstphoto:string
    }


    @observable done: boolean;

    componentWillMount() {
        const {done} = this.props;
        this.done = !!done;
    }

    @autobind @action
    toggle() {
         const {firstname,secondname,firstvotes,secondvotes,firstphoto,secondphoto,duel_id,navigation} = this.props;
         
         let data ={
            firstname:firstname,
            secondname:secondname,
            firstvotes:firstvotes,
            secondvotes:secondvotes,
            firstphoto:firstphoto,
            secondphoto:secondphoto,
            duel_id:duel_id
         }
         navigation.navigate("Viewdualprofile",{data});
    }

    render(): React$Element<*>  {
        const {firstname,secondname,firstvotes,secondvotes,navigation} = this.props;
        const btnStyle ={ backgroundColor: this.done ? variables.brandInfo : variables.lightGray };
        return <View style={Styles.listItem}>

            <TouchableOpacity style={[Styles.center, style.title, style.topnames]} onPress={this.toggle}>
                <View>
                    <Text style={style.firstname}>{firstname}</Text>
                    <Text style={style.votes}>{firstvotes} Votes</Text>
                </View>
                <View>
                    <Text style={style.vs}>VS</Text>
                </View>
                <View>
                    <Text style={style.secondname}>{secondname}</Text>
                    <Text style={style.votes}>{secondvotes} Votes</Text>
                </View>
            </TouchableOpacity>


        </View>;

    }
}


const style = StyleSheet.create({
    mask: {
        backgroundColor: "rgba(0, 0, 0, .5)"
    },
    button: {
        height: 75, width: 75, borderRadius: 0
    },
    title: {
        paddingLeft: variables.contentPadding,
        padding:20
    },
    userrank:{
        flex:2, 
        left: 0, 
        right: 0, 
        bottom: 0
    },
    topnames:{
        flex:1,
        flexDirection:'row',
        justifyContent: 'space-between',
    },
    firstname:{
        color:'#635DB7'
    },
    secondname:{
        color:'#00CE9F'
    },
    votes:{
        fontWeight:'bold'
    },
    vs:{
        fontWeight:'bold',
        fontSize:20,
    },
        insert:{
      color:'#fff',
      fontSize:25
    },
        img: {
        resizeMode: "cover",
        ...WindowDimensions
    },
            imgMask: {
        backgroundColor: "rgba(71, 255, 247, .4)"
    }
});