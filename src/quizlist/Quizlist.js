// @flow
import autobind from "autobind-decorator";
import React, {Component} from "react";
import {StyleSheet, Image, View, Text, TouchableOpacity, Footer,ActivityIndicator, ScrollView} from "react-native";
import {H1,H3, Button, Icon} from "native-base";
import {observable, action} from "mobx";
import { observer } from "mobx-react/native";
import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";

import {BaseContainer, Styles, Images ,Avatar, WindowDimensions} from "../components";

import variables from "../../native-base-theme/variables/commonColor";
import Small from "../components/Small";
import api from '../Utils/api';
import helper from '../Utils/helper';

export default class Quizlist extends Component {
   
   constructor(props) {
    super(props)

    this.state = {
      loading: true,
      jsonData:[],
      currentuserid:'',
       selected: false
    }

    console.log('stateparams==');
       
  }

   onSelect = data => {
    console.log('selctstateasss');
    this.setState({loading:true});
    this.getData(this.userData.user_id);
  };
    componentWillMount() {

        helper.getCache('@userdata').then((res) => {

            console.log(res);
            if(res){

                this.userData = JSON.parse(res);

                this.getData(this.userData.user_id);
                
            }else{

                this.setState({loading:false}); 
            }
            
           
        })

        
    //const userdata = await AsyncStorage.getItem('@userdata'); 
    }



    getData(user_id){

            let data ={
                user_id:user_id
            }

            console.log(data);
            api.postApi('quiz_list_service.php',data)
                  .then((res) => {

                    
                    if(res.Result=='True'){
                       // console.log(res.data);
                        this.setState({loading:false,jsonData:res.data,currentuserid:user_id}); 

                    }else{

                         this.setState({loading:false}); 
                    }
                   
            })
            .catch((e) => {

                     //alert(e.message);
                //this.refs.toast.show(e.message);
                this.setState({loading:false});

            })
    }
     @autobind
    go() {
        this.props.navigation.navigate('Addquiz', { onSelect: this.onSelect });
    }

    render(): React$Element<*> {
        const {jsonData, loading,currentuserid} = this.state;
        if (loading) {

            return <Image source={Images.drawer} style={styles.img} ><BaseContainer title="Quiz List" navigation={this.props.navigation}>
                                                
            <ScrollView style={styles.imgMask }>
            {
                    <View style={ styles.flex }>
                      <ActivityIndicator
                        animating={ loading }
                        style={[
                          styles.centering,
                          { height: 80 }
                        ]} size="large" />
                    </View>
                }
                
                                                               </ScrollView>
                
                </BaseContainer></Image>;
            

        }else{

            return <Image source={Images.drawer} style={styles.img} ><BaseContainer title="Quiz List" navigation={this.props.navigation} >
                                                
            <ScrollView style={styles.imgMask }>

            {
                !loading && <View>

                    {
                        jsonData.length==0 && <View>
                              <Text style={{flex:1, flexDirection:'row',fontSize:20,padding:20,
         textAlign:'center'}}>No Records Found</Text>
                        </View>
                    }
                    {
                        _.map(
                            jsonData,
                            (item, key) => <Item
                                key={key}
                                serailid={key}
                                navigation={this.props.navigation}
                                answer_a={item.answer_a}
                                answer_b={item.answer_b}
                                answer_c={item.answer_d}
                                answer_d={item.answer_d}
                                quiz_id={item.quiz_id}
                                question={item.question}
                                currentuserid={currentuserid}
                                userid={item.user_id}
                            />
                        )
                    }
                          
                </View>
            }

            </ScrollView>
                
              <Button primary full onPress={this.go}>
                   <Text style={styles.insert}><Icon name="md-add"/> Insert</Text>
              </Button>
            </BaseContainer></Image>;
        }
        
        
    }
}


@observer
class Item extends Component {

    props: {
        
        question:string,
        userid:number,
        currentuserid:number,
        navigation:any,
        serailid:any
    }


    @autobind @action
    toggle() {
         console.log('ffdfg');
         const {navigation,currentuserid,question,quiz_id,answer_a,answer_b,answer_c,answer_d} = this.props;
         navigation.navigate("Quiz",{userid:currentuserid,question:question,quiz_id:quiz_id,answer_a:answer_a,answer_b:answer_b,answer_c:answer_c,answer_d:answer_d});
    }

    render(): React$Element<*>  {
        const {userid,question,serailid} = this.props;
        const btnStyle ={ backgroundColor: this.done ? variables.brandInfo : variables.lightGray };
        return <View style={Styles.listItem}>

            <TouchableOpacity style={[Styles.center, styles.title, styles.topnames]}  onPress={this.toggle}>
                
              <View style={styles.toplist}>   
                  <View style={styles.lefttop}>
                      <Text style={styles.gray}>{serailid+1}</Text><H3 style={styles.top_name}> {question}</H3> 
                  </View>
              </View>
            </TouchableOpacity>

            

        </View>;

    }
}


const styles = StyleSheet.create({
    mask: {
        backgroundColor: "rgba(0, 0, 0, .5)"
    },
    button: {
        height: 75, width: 75, borderRadius: 0
    },
    title: {
        paddingLeft: variables.contentPadding,
        padding:20
    },
    userrank:{
        flex:2, 
        left: 0, 
        right: 0, 
        bottom: 0
    },
    namebold:{
        fontWeight:'bold',
                color:'#00CE9F'
    },
    namebold:{
        fontWeight:'bold',
                color:'#00CE9F'
    },
    topnames:{
        flex:1,
        flexDirection:'row',
        justifyContent: 'space-between',
    },
    name:{
        fontSize:16
    },
    activename:{
        backgroundColor:'#635DB7'
    },
    activecolor:{
        color:'#fff'
    },
    title: {
        justifyContent: "center",
        flex: 1,
        padding: variables.contentPadding
    },
        gray: {
        color: variables.gray,
        fontSize:20
    },
    toplist:{
        flex:1,
        flexDirection:'row',
        justifyContent: 'space-between',
    },
    top_name:{
        lineHeight:20,
        color:'#635DB7'
    },

    lefttop:{
        flex:1,
        flexDirection:'row',
        paddingTop:10,
        paddingBottom:10
    },
    top_vp:{
        color:'#00CE9F'
    },
    insert:{
      color:'#fff',
      fontSize:25
    },
                img: {
        resizeMode: "cover",
        ...WindowDimensions
    },
            imgMask: {
        backgroundColor: "rgba(71, 255, 247, .4)"
    }


});