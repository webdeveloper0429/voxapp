// @flow
import autobind from "autobind-decorator";
import React, {Component} from "react";
import {StyleSheet, Image, View, Text, TouchableOpacity, Footer,ActivityIndicator, ScrollView} from "react-native";
import {H1,H3, Button, Icon} from "native-base";
import {observable, action} from "mobx";
import { observer } from "mobx-react/native";
import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";

import {BaseContainer, Styles, Images ,Avatar, WindowDimensions} from "../components";

import variables from "../../native-base-theme/variables/commonColor";
import Small from "../components/Small";

import api from '../Utils/api';
import helper from '../Utils/helper';

export default class Top extends Component {
   
    constructor(props) {
        super(props)

        this.state = {
          loading: true,
          top10:[],
         currentuserid:'',
        }
  
    }


    componentWillMount() {

        helper.getCache('@userdata').then((res) => {

           
            if(res){

                this.userData = JSON.parse(res);

                this.getData(this.userData.user_id);
                
            }else{

                this.setState({loading:false}); 
            }
            
           
        })

        
    //const userdata = await AsyncStorage.getItem('@userdata'); 
    }

    onSelect = data => {
        console.log('selctstateasss');
        this.setState({loading:true});
        this.getData(this.userData.user_id);
   };

   onVote = data => {
        console.log('data',data);
        this.setState({voteloading:true});
       
   };

    getData(user_id){

            let data ={
                user_id:user_id
            }

            api.postApi('top10_list_service.php',data)
                  .then((res) => {

                    console.log(res);
                    if(res.Result=='True'){
                        
                        for(let list in res.data){

                            res.data[list].voting=false;

                        }


                        this.setState({loading:false,top10:res.data,currentuserid:user_id}); 
                    }else{

                         this.setState({loading:false}); 
                    }
                   
            })
            .catch((e) => {

                     //alert(e.message);
                //this.refs.toast.show(e.message);
                this.setState({loading:false});

            })
    }

     @autobind
    go() {
        this.props.navigation.navigate('Addtop', { onSelect: this.onSelect });
    }



    render(): React$Element<*> {
        const {top10, loading,currentuserid} = this.state;
        if (loading) {

            return <BaseContainer title="Top" navigation={this.props.navigation} scrollable>
            <Image source={Images.login} style={styles.img} >
            <ScrollView style={styles.imgMask }>
            {
                    <View style={ Styles.flex }>
                      <ActivityIndicator
                        animating={ loading }
                        style={[
                          Styles.centering,
                          { height: 80 }
                        ]} size="large" />
                    </View>
                }
                </ScrollView>
                </Image>
                </BaseContainer>;
            

        }else{

            return <Image source={Images.drawer} style={styles.img} ><BaseContainer title="Top" navigation={this.props.navigation} >
            
            <ScrollView style={styles.imgMask }>
            {
                !loading && <View>

                    {
                        top10.length==0 && <View>
                           <Text style={{flex:1, flexDirection:'row',fontSize:20,padding:20,
         textAlign:'center'}}>No Records Found</Text>
                        </View>
                    }
                    {
                        _.map(
                            top10,
                            (item, key) => <Item
                                key={key}
                                selectkey={key}
                                vote={item.voting}
                                navigation={this.props.navigation}
                                name={item.top_name}
                                rank={item.user_rank}
                                image={item.top_image}
                                token={item.vp_token}
                                currentuserid={currentuserid}
                                userid={item.user_id}
                            />
                        )
                    }
        
                </View>


            }

             </ScrollView>
                           <Button primary full onPress={this.go}>
                   <Text style={styles.insert}><Icon name="md-add"/> Insert</Text>
              </Button>
              
            
            </BaseContainer></Image>;
        }
        
        
    }
}


@observer
class Item extends Component {

    props: {
        name: string,
        rank:sting,
        userid:number,
        navigation:any
        
    }


    @autobind @action
    toggle() {
         const {userid,navigation,name,currentuserid,selectkey} = this.props;
         navigation.navigate("Topdetails",{name:name,userid:userid,currentuserid:currentuserid,key:selectkey});
    }

    @autobind @action
    vote(){

        const {userid,navigation,name,currentuserid,selectkey,voting} = this.props;
        console.log('voting===',voting);
    }

    render(): React$Element<*>  {
        const {name,rank,userid,currentuserid,image,token,item} = this.props;
        const btnStyle ={ backgroundColor: this.done ? variables.brandInfo : variables.lightGray };
        return <View style={Styles.listItem} >

            {currentuserid!=userid&&<TouchableOpacity onPress={this.toggle} style={[Styles.center, styles.title, styles.topnames]} >
                
            <View style={styles.toplist}>   
                <View style={styles.lefttop}  >
                    <H3 style={styles.top_name}>{name}</H3>
                    <Text style={styles.gray}>{rank}</Text>
                </View>
                <View style={styles.middletop} >
                    {image!=''&&<Image style={{width:50,height:50}} source={{uri:image}}/>}
                    {image==''&&<Avatar size={50}/>}
                </View>
                 <View style={{ flexDirection:'row'}}>
                    <H3 style={styles.top_vp}>{token}</H3>
                    <Button  onPress={this.vote} primary style={styles.vpbtns}><Text style={styles.vpbtn}>VP</Text></Button>
                </View>
            </View>
             
            </TouchableOpacity>}

             {currentuserid==userid&&<TouchableOpacity style={[Styles.center, styles.title, styles.topnames, styles.activename]}  onPress={this.toggle}>
                
                
            <View style={[styles.toplist, styles.activecolor]}>   
                <View style={styles.lefttop}>
                    <H3 style={styles.top_name}>{name}</H3>
                    <Text style={styles.gray}>{rank}</Text>
                </View>
                <View style={styles.middletop}>
                     {image!=''&&<Image style={{width:50,height:50}} source={{uri:image}}/>}
                {image==''&&<Avatar size={50}/>}
                </View>
                <View style={{ flexDirection:'row'}}>
                    <H3 style={styles.top_vp}>{token} </H3>
                    <Button onPress={this.vote} primary style={styles.vpbtns}><Text style={styles.vpbtn}>VP</Text></Button>
                </View>
            </View>
            </TouchableOpacity>}

        </View>;

    }
}


const styles = StyleSheet.create({

    img: {
        resizeMode: "cover",
        ...WindowDimensions
    },
    imgMask: {
        backgroundColor: "rgba(71, 255, 247, .4)"
    },
    button: {
        height: 75, width: 75, borderRadius: 0
    },
    title: {
        paddingLeft: variables.contentPadding,
        padding:20
    },
    userrank:{
        flex:2, 
        left: 0, 
        right: 0, 
        bottom: 0
    },
    namebold:{
        fontWeight:'bold',
                color:'#00CE9F'
    },
    namebold:{
        fontWeight:'bold',
                color:'#00CE9F'
    },
    topnames:{
        flex:1,
        flexDirection:'row',
        justifyContent: 'space-between',
    },
    name:{
        fontSize:16
    },
    activename:{
        backgroundColor:'#635DB7'
    },
    activecolor:{
        
    },
    title: {
        justifyContent: "center",
        flex: 1,
        padding: variables.contentPadding
    },
        gray: {
        color: variables.gray
    },
    toplist:{
        flex:1,
        flexDirection:'row',
        justifyContent: 'space-between',
    },
    top_name:{
        lineHeight:20,
        color:'#635DB7'
    },

    lefttop:{
        width:130
    },
    middletop:{
        width:40
    },
    top_vp:{
        color:'#635DB7',
        fontWeight:'bold',
        paddingRight:5
    },
    insert:{
      color:'#fff',
      fontSize:25
    },
    vpbtns:{
        padding:1
    },
    vpbtn:{
        color:'#fff',
        fontSize:18
    }




});