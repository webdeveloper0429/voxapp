// @flow
import autobind from "autobind-decorator";
import React from "react";
import {View, Image, StyleSheet, ScrollView, KeyboardAvoidingView,AsyncStorage} from "react-native";
import {H1, Container, Button, Text,Spinner} from "native-base";
import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";

import Mark from "./Mark";
import {Small, Styles, Images, Field, NavigationHelpers, WindowDimensions} from "../components";
import variables from "../../native-base-theme/variables/commonColor";


import api from '../Utils/api';
import helper from '../Utils/helper';

import Toast,{DURATION} from "react-native-easy-toast";

export default class Login extends React.Component {

    constructor(props) {
    super(props)

    this.state = {
      isLoading: false,
      email:'',
      password:''
    }

       
  }
  
    props: {
        navigation: NavigationScreenProp<*, *>
    }

    @autobind
    forgotPassword() {
        //this.props.navigation.navigate("ForgotPassword");
    }
    
    @autobind 
    signIn(){
        try {

             var email = this.state.email;
             var password = this.state.password;
             var emailRegex = /^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$/;

             if(email==''){

                this.refs.toast.show('Please provide email address');

             }else if(password==''){

                this.refs.toast.show('Please provide password');
             }
             else if(!emailRegex.test(email)){

                this.refs.toast.show('Email format is invalid');

             }else{

                this.setState({isLoading:true});
                let data ={
                    'user_email':email,
                    'user_password':password
                }
                api.postApi('user_login_service.php',data)
                  .then((res) => {

                    //console.log(res);
                    

                    if(res.Result=='True'){

                        
                         helper.setCache('@userdata',res.data);
                         this.checkIn(res.data.user_id);
                        

                    }else{
                        this.setState({isLoading:false});
                        this.refs.toast.show(res.ResponseMsg);
                    }

                    
                    
                  })
                  .catch((e) => {
                     //alert(e.message);
                     this.refs.toast.show(e.message);
                     this.setState({isLoading:false});
                  })


             }

             

        } catch(e) {
            this.setState({isLoading:false});
            this.refs.toast.show(e.message);
        }  
    }

    checkIn(user_id){

                let data ={
                    'user_id':user_id,
                    'spend_type':'I'
                }
                console.log(data);
                api.postApi('add_time_service.php',data)
                  .then((res) => {

                    console.log(res);
                    

                   this.setState({isLoading:false});
                        NavigationHelpers.reset(this.props.navigation, "Main");
                    
                  })
                  .catch((e) => {
                     //alert(e.message);
                     console.log(e.message);
                     //this.refs.toast.show(e.message);
                     //this.setState({isLoading:false});
                  })


             }

    

    @autobind
    signUp() {
        this.props.navigation.navigate("SignUp");
    }

    @autobind
    fbSignIn(){

        NavigationHelpers.reset(this.props.navigation, "Main");
    }

    @autobind
    googleSignIn(){

        //NavigationHelpers.reset(this.props.navigation, "Main");
    }

   

    render(): React$Element<*> {
        return <Image source={Images.login} style={style.img}>
            <Container style={StyleSheet.flatten(Styles.imgMask)}>
                <Toast ref="toast"/> 
                <ScrollView >
                    <KeyboardAvoidingView behavior="position">
                        <View style={style.logo}>
                            <View>
                                <Mark />
                                <H1 style={StyleSheet.flatten(style.title)}>Get Started!</H1>
                            </View>
                        </View>
                        <View style={style.blur} >
                            <Field
                                label="Email"
                                autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="email-address"
                                returnKeyType="next"
                                value={this.state.email}
                                onChangeText={(email) => this.setState({email})}
                                inverse
                            />
                            <Field
                                label="Password"
                                secureTextEntry
                                autoCapitalize="none"
                                autoCorrect={false}
                                returnKeyType="go"
                                value={this.state.password}
                                onChangeText={(password) => this.setState({password})}
                                onSubmitEditing={this.signIn}
                                last
                                inverse
                            />

                            


                        </View>


                    </KeyboardAvoidingView>

                                <View style={style.signinbutton}>
                                    <Button primary full onPress={this.signIn}>
                                    {this.state.isLoading ? <Spinner color="white" /> : <Text>Sign In</Text>}
                                    </Button>
                                </View>

                                <View style={style.socialbtn}>
                                    <View style={style.socialbtninner} >
                                        <Button style={style.fb} onPress={this.fbSignIn}>
                                            <Text>Facebook Login</Text>
                                        </Button>
                                    </View>

                                    <View style={style.socialbtninner}>
                                        <Button style={style.google} onPress={this.googleSignIn}>
                                            <Text>Google Login</Text>
                                        </Button>
                                    </View>
                                </View>
                                <View>
                                    <Button transparent full onPress={this.signUp}>
                                        <Small style={{color: "white"}}>Don't have an account? Sign Up</Small>
                                    </Button>
                                </View>
                                <View>
                                    <Button transparent full onPress={this.forgotPassword}>
                                        <Small style={{color: "white"}}>Forgot password?</Small>
                                    </Button>
                                </View>
                </ScrollView>
            </Container>
        </Image>;
    }
}

const style = StyleSheet.create({
    img: {
        resizeMode: "cover",
        ...WindowDimensions
    },
    content: {
        flex: 1,
        justifyContent: "flex-end"
    },
    logo: {
        alignSelf: "center",
        marginBottom: variables.contentPadding * 1,
    },
    title: {
        marginTop: variables.contentPadding * 2,
        color: "white",
        textAlign: "center"
    },
    blur: {
        backgroundColor: "rgba(255, 255, 255, .2)"
    },
    socialbtn: {
        flex:2,
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    socialbtninner:{
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft:10,
        marginRight:10
    },
    fb:{
        backgroundColor:'#4166b2'
    },
    google:{
        backgroundColor:"#ea433b"
    },
    signinbutton:{
        margin:10
    }

});
