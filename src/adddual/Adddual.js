// @flow
import autobind from "autobind-decorator";
import React, {Component} from "react";
import {View, Image, StyleSheet, KeyboardAvoidingView, ScrollView ,Text, Picker,TouchableOpacity} from "react-native";
import {H1} from "native-base";
import {Container, Button, Header, Left, Right, Body, Icon, Title,Spinner} from "native-base";
import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";
import { EvilIcons } from "@expo/vector-icons";
import { Col, Row, Grid } from 'react-native-easy-grid';
import {BaseContainer, Avatar, TaskOverview, Small, Styles,Images, Task, Field, NavigationHelpers,WindowDimensions} from "../components";

import variables from "../../native-base-theme/variables/commonColor";

import api from '../Utils/api';
import helper from '../Utils/helper';

import Toast,{DURATION} from "react-native-easy-toast";

import { ImagePicker } from 'expo';

export default class Adddualprofile extends Component {

    constructor(props) {
        super(props)

        this.image_path = 'http://voxapp.studiowebdemo.com/services/images/user_image/';
        this.imagechange= false;
        this.state = {
            user_id: '',
            firstname:'',
            secondname:'',
            firstphoto:'',
            secondphoto:'',
            token:0
        }

       
  }

     componentWillMount() {

        helper.getCache('@userdata').then((res) => {

            console.log(res);
            if(res){

                this.userData = JSON.parse(res);
               

                this.setState({

                    isLoading: false,
                    token:this.userData.tokens
                   
                })
                
            }else{

                this.setState({loading:false}); 
            }
            
           
        })

        
    //const userdata = await AsyncStorage.getItem('@userdata'); 
    }

    @autobind
    addDuel() {
        try {
            
             var firstname = this.state.firstname;
             var secondname = this.state.secondname;
             var firstphoto = this.state.firstphoto;
             var secondphoto = this.state.secondphoto;
            
            if(firstname==''){

                this.refs.toast.show('Please provide firstname');

             }else if(secondname==''){

                this.refs.toast.show('Please provide secondname');

             }else if(firstphoto==''){

                this.refs.toast.show('Please provide firstphoto');

             }else if(secondphoto==''){

                this.refs.toast.show('Please provide secondphoto');

             }else{

                this.setState({isLoading:true});
                
                let data;

                    let imagedata1 ={
                        uri:firstphoto,
                        name:'profile.jpg',
                        type:'image/jpg'
                    }

                    let imagedata2 ={
                        uri:secondphoto,
                        name:'profile1.jpg',
                        type:'image/jpg'
                    }

                    data ={
                        user_id:this.userData.user_id,
                        firstname:firstname,
                        secondname:secondname,
                        firstphoto:imagedata1,
                        secondphoto:imagedata2
                       
                    }
                

                console.log(data);
                api.postApi('add_duel_service.php',data)
                  .then((res) => {

                    console.log(res);
                    this.setState({isLoading:false});

                    if(res.Result=='True'){


                        //this.refs.toast.show(res.ResponseMsg);
                        //this.props.navigation.navigate("QuizList");
                        const { navigation } = this.props;
                        navigation.goBack();
                        navigation.state.params.onSelect({ selected: true });

                    }else{

                        this.refs.toast.show(res.ResponseMsg);
                        
                    }

                    
                    
                  })
                  .catch((e) => {
                    // alert(e.message);
                     this.refs.toast.show(e.message);
                     this.setState({isLoading:false});
                  })


             }

             

        } catch(e) {
            this.setState({isLoading:false});
            this.refs.toast.show(e.message);
        } 
    }
    
     @autobind
    async getSecondImage(): Promise<void> {
        console.log('camera start');
     
        let result = await ImagePicker.launchImageLibraryAsync({
            allowEditing:true,
            aspect:[4,3]
        })

        //console.log(result);

        if(!result.cancelled){

            console.log(result.uri);
           // this.imagechange =true;
            this.setState({secondphoto:result.uri});
        }
    }

     @autobind
    async getFirstImage(): Promise<void> {
        console.log('camera start');
     
        let result = await ImagePicker.launchImageLibraryAsync({
            allowEditing:true,
            aspect:[4,3]
        })

        //console.log(result);

        if(!result.cancelled){

            console.log(result.uri);
           // this.imagechange =true;
            this.setState({firstphoto:result.uri});
        }
    }

    props: {
        navigation: NavigationScreenProp<*, *>
    }

    @autobind
    back() {
        this.props.navigation.goBack();
    }

    @autobind
    signIn() {
        NavigationHelpers.reset(this.props.navigation, "Walkthrough");
    }

    render(): React$Element<*> {
        return <Container>
         <Image source={Images.login} style={style.img} >
            <Header noShadow>
                <Left>
                    <Button onPress={this.back} transparent>
                        <Icon name='close' />
                    </Button>
                </Left>
                <Body>
                    <Title>Add Dual</Title>
                </Body>
                <Right />
            </Header>
            <ScrollView style={style.imgMask} >
            <View style={{ flex: 1, flexDirection:'row' }}>
                <View style={{ flex: 1, backgroundColor: '#635DB7', height: 200 }}>
                    <KeyboardAvoidingView behavior="position">
                    <Field label="Name"  autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="default"
                                returnKeyType="next"
                                value={this.state.firstname}
                                onChangeText={(firstname) => this.setState({firstname})}
/>
                    </KeyboardAvoidingView>
                    
                    <Col style={{ justifyContent: 'center', alignItems: 'center' }}>  
                       
                        <TouchableOpacity onPress={this.getFirstImage}>
                         {this.state.firstphoto!=''&&<Image source={{uri:this.state.firstphoto}} style={{width:100,height:100, justifyContent: 'center', alignItems: 'center'}}/>}
                        {this.state.firstphoto==''&&<Avatar size={100} style={{ justifyContent: 'center', alignItems: 'center'}}/>}

                         </TouchableOpacity>
                        
                    </Col>
                </View>
                <View style={{ flex: 1, backgroundColor: '#00CE9F', height: 200 }}>
                    <KeyboardAvoidingView behavior="position">
                    <Field label="Name" autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="default"
                                returnKeyType="next"
                                value={this.state.secondname}
                                onChangeText={(secondname) => this.setState({secondname})}
/>
                    </KeyboardAvoidingView>
                    
                    <Col style={{ justifyContent: 'center', alignItems: 'center' }}>  
                         <TouchableOpacity onPress={this.getSecondImage}>
                         {this.state.secondphoto!=''&&<Image source={{uri:this.state.secondphoto}} style={{width:100,height:100, justifyContent: 'center', alignItems: 'center'}}/>}
                        {this.state.secondphoto==''&&<Avatar size={100} style={{ justifyContent: 'center', alignItems: 'center'}}/>}

                         </TouchableOpacity>
                    </Col>
                </View>
            </View>

            <Grid>
            <Col style={{ height: 100, justifyContent: 'center',
                alignItems: 'center' }}>  
                <Text style={{padding:20, borderRadius:100, backgroundColor: '#635DB7', color: '#fff', fontSize:20, fontWeight:'bold'}}>VP</Text>
            </Col>
            <Col style={{ padding:10, borderRadius:100, height: 100, justifyContent: 'center',
                alignItems: 'center' }}>
                <Text style={{padding:20, borderRadius:100, backgroundColor: '#00CE9F', color: '#fff', fontSize:20, fontWeight:'bold'}}>VP</Text>
            </Col>
          </Grid>
          <Grid>
            <Col style={{ height: 100, justifyContent: 'center',
                alignItems: 'center' }}>  
                <Text style={{color: 'red', fontSize:30, fontWeight:'bold'}}>Points</Text>
            </Col>
          </Grid>
  
            </ScrollView>

            <Text style={{backgroundColor: "rgba(71, 255, 247, .4)", color: '#00CE9F', fontSize:22, padding:10, fontWeight:'bold'}}>VP : {this.state.token}</Text>

            <Button primary full style={{ height: variables.footerHeight }} onPress={this.addDuel}>
               {this.state.isLoading ? <Spinner color="white"/> : <Text style={{ color:'#fff' }}>SAVE</Text>} 
            </Button>
            <Toast ref="toast"/>
            </Image> 
        </Container>;
    }
}

const style = StyleSheet.create({
 heading: {
        marginTop: variables.contentPadding * 2,
        color: "white"
    },
      date:{
    flexDirection: 'row',
    height:550
  },
  rating:{
    textAlign:'center', 
    margin:12
  },
ratinginner:{
    alignItems: 'center', 
    justifyContent: 'center',
    width:60, 
    height:60, 
    textAlign:'center',
    padding:20,
    borderRadius: 100
  },
  ratingtext:{
    color:'#fff',
    fontSize:16
  },
          img: {
        resizeMode: "cover",
        ...WindowDimensions
    },
            imgMask: {
        backgroundColor: "rgba(71, 255, 247, .4)"
    }
});