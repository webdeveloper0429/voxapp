// @flow
import moment from "moment";
import autobind from "autobind-decorator";
import React, {Component} from "react";
import {StyleSheet, Image, View, Text, ScrollView, Dimensions,ActivityIndicator} from "react-native";
import {H1,Container, Button, Header, Left, Right, Body, Icon, Title,Spinner} from "native-base";

import {BaseContainer, Styles, Images, Avatar, Task, WindowDimensions} from "../components";
import { Col, Row, Grid } from 'react-native-easy-grid';
import variables from "../../native-base-theme/variables/commonColor";

import Toast,{DURATION} from "react-native-easy-toast";


import api from '../Utils/api';
import helper from '../Utils/helper';

export default class Wallet extends Component {
    
    constructor(props) {
    super(props)

        this.state = {
          loading: false,
          token:0

        }

       console.log('params====',this.props.navigation.state.params);
    }

    componentWillMount() {

        helper.getCache('@userdata').then((res) => {

            console.log(res);
            if(res){

                this.userData = JSON.parse(res);

                this.setState({loading:false,token:this.userData.tokens}); 
                
            }else{

                this.setState({loading:false}); 
            }
            
           
        })

        
    //const userdata = await AsyncStorage.getItem('@userdata'); 
    }

    
    selectCoin = (coin,coinname) => {

        console.log(coin,coinname);
        this.buyCoin(coin);
    }

    buyCoin(coin){

        let data ={
                user_id:this.userData.userid,
                amount:coin,
                number_of_coin:coin
            }

            this.setState({loading:true})

            api.postApi('user_payment_service.php',data)
                  .then((res) => {

                    console.log(res);
                    if(res.Result=='True'){
                       
                        this.setState({loading:false}); 
                        this.refs.toast.show(res.ResponseMsg);

                    }else{

                          this.refs.toast.show(res.ResponseMsg);
                         this.setState({loading:false}); 
                    }
                   
            })
            .catch((e) => {

                     //alert(e.message);
                //this.refs.toast.show(e.message);
                this.setState({loading:false});

            })

    }

    @autobind
    back() {
        this.props.navigation.goBack();
    }

    render(): React$Element<*> {
        const screenHeight = Dimensions.get('window').height
        return <Image source={Images.drawer} style={style.img}>
        <Container style={StyleSheet.flatten(style.container)}>
            <Header noShadow>
                <Left>
                    <Button onPress={this.back} transparent>
                        <Icon name='close' />
                    </Button>
                </Left>
                <Body>
                    <Title>Buy Wallet</Title>
                </Body>
                <Right />
            </Header>
            <View >
                <Grid>
                    <Col style={{  height: screenHeight,alignItems: 'center'}}>  
                        <Text style={{marginTop:10, marginBottom:20, width:70, height:70, borderRadius:100, 
                            backgroundColor: '#fff', color: '#635DB7', fontSize:20, 
                            paddingTop:20 , fontWeight:'bold', textAlign:'center'}}>VP</Text>
                            <Text style={{color:'#fff', fontSize:18}}>{this.state.token} Coins</Text>
                            
                            <Text style={{marginTop:10, width:70, height:70, backgroundColor: '#00CE9F', color: '#fff', borderRadius:5, fontSize:20, 
                             padding:10 , fontWeight:'bold', textAlign:'center'}}  onPress={() => this.selectCoin('100','vp')} >100 Coins</Text>
                             <Text style={{marginTop:10, width:70, height:70, backgroundColor: '#00CE9F', color: '#fff', borderRadius:5, fontSize:20, 
                             padding:10 , fontWeight:'bold', textAlign:'center'}}  onPress={() => this.selectCoin('200','vp')} >200 Coins</Text>
                            <Text style={{marginTop:10, width:70, height:70, backgroundColor: '#00CE9F', color: '#fff', borderRadius:5, fontSize:20, 
                             padding:10 , fontWeight:'bold', textAlign:'center'}}  onPress={() => this.selectCoin('500','vp')} >500 Coins</Text>                            
                    </Col>

                    <Col style={{height: screenHeight, alignItems: 'center'}}>
                        <Text style={{marginTop:10, marginBottom:20, width:70, height:70, borderRadius:100,
                          backgroundColor: '#fff', color: '#00CE9F',fontSize:20, paddingTop:20,
                          fontWeight:'bold', textAlign:'center'}}>PVP</Text>
                          <Text style={{color:'#fff', fontSize:18}}>0 Coins</Text>
                        <Text style={{marginTop:10, width:70, height:70, backgroundColor: '#635DB7', color: '#fff', borderRadius:5, fontSize:20,
                          padding:10, fontWeight:'bold', textAlign:'center'}}  onPress={() =>this.selectCoin('1000','pvp')}>1000 Coins</Text>
                           <Text style={{marginTop:10, width:70, height:70, backgroundColor: '#635DB7', color: '#fff', borderRadius:5, fontSize:20,
                          padding:10, fontWeight:'bold', textAlign:'center'}}  onPress={() =>this.selectCoin('2000','pvp')}>2000 Coins</Text>
                           <Text style={{marginTop:10, width:70, height:70, backgroundColor: '#635DB7', color: '#fff', borderRadius:5, fontSize:20,
                          padding:10, fontWeight:'bold', textAlign:'center'}}  onPress={() =>this.selectCoin('5000','pvp')}>5000 Coins</Text>
                    </Col>
                </Grid>

                               
            </View>
            <Toast ref="toast"/> 
            </Container>
            </Image>;
    }
}

const style = StyleSheet.create({
    heading: {
        marginTop: variables.contentPadding * 2,
        color: "white"
    },
        img: {
        resizeMode: "cover",
        ...WindowDimensions
    },
    container: {
        backgroundColor: "rgba(80, 210, 194, .5)"
    },
});