// @flow
import autobind from "autobind-decorator";
import React, {Component} from "react";
import {View, Image, StyleSheet, KeyboardAvoidingView, ScrollView ,Text, Picker,TouchableOpacity} from "react-native";
import {H1} from "native-base";
import {Container, Button, Header, Left, Right, Body, Icon, Title,Spinner} from "native-base";
import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";
import { EvilIcons } from "@expo/vector-icons";
import { Col, Row, Grid } from 'react-native-easy-grid';
import {BaseContainer, Avatar, TaskOverview, Small, Styles, Task, Field, NavigationHelpers, WindowDimensions, Images} from "../components";

import variables from "../../native-base-theme/variables/commonColor";

import api from '../Utils/api';
import helper from '../Utils/helper';

import Toast,{DURATION} from "react-native-easy-toast";

import { ImagePicker } from 'expo';

export default class Addtop extends Component {

    constructor(props) {
        super(props)

        this.image_path = 'http://voxapp.studiowebdemo.com/services/images/user_image/';
        this.imagechange= false;
        this.state = {
            user_id: '',
            top_name:'',
            image_file:''
        }
    }

    componentWillMount() {

        helper.getCache('@userdata').then((res) => {

            console.log(res);
            if(res){

                this.userData = JSON.parse(res);
               

                this.setState({

                    isLoading: false,
                    token:this.userData.tokens
                   
                })
                
            }else{

                this.setState({loading:false}); 
            }
            
           
        })

        
    //const userdata = await AsyncStorage.getItem('@userdata'); 
    }

    @autobind
    addTop() {
        try {
            
             var top_name = this.state.top_name;
             var image_file = this.state.image_file;
             
            
            if(top_name==''){

                this.refs.toast.show('Please provide name');

             }else if(image_file==''){

                this.refs.toast.show('Please provide image');

             }else{

                this.setState({isLoading:true});
                
                let data;

                    let imagedata1 ={
                        uri:image_file,
                        name:'profile.jpg',
                        type:'image/jpg'
                    }

                    

                    data ={
                        user_id:this.userData.user_id,
                        top_name:top_name,
                        image_file:imagedata1
                       
                    }
                

                //console.log(data);
                api.postApi('add_top10_service.php',data)
                  .then((res) => {

                   // console.log('ewrew',res);
                    this.setState({isLoading:false});

                    if(res.Result=='True'){


                        //this.refs.toast.show(res.ResponseMsg);
                        //this.props.navigation.navigate("QuizList");
                        const { navigation } = this.props;
                        navigation.goBack();
                        navigation.state.params.onSelect({ selected: true });

                    }else{

                        this.refs.toast.show(res.ResponseMsg);
                        
                    }

                    
                    
                  })
                  .catch((e) => {
                    // alert(e.message);
                     this.refs.toast.show(e.message);
                     this.setState({isLoading:false});
                  })


             }

             

        } catch(e) {
            this.setState({isLoading:false});
            this.refs.toast.show(e.message);
        } 
    }
    
    
     @autobind
    async getFirstImage(): Promise<void> {
        console.log('camera start');
     
        let result = await ImagePicker.launchImageLibraryAsync({
            allowEditing:true,
            aspect:[4,3]
        })

        //console.log(result);

        if(!result.cancelled){

            console.log(result.uri);
           // this.imagechange =true;
            this.setState({image_file:result.uri});
        }
    }

    props: {
        navigation: NavigationScreenProp<*, *>
    }

    @autobind
    back() {
        this.props.navigation.goBack();
    }

   

    render(): React$Element<*> {
        return <Image source={Images.login} style={style.img} ><Container>
            <Header noShadow>
                <Left>
                    <Button onPress={this.back} transparent>
                        <Icon name='close' />
                    </Button>
                </Left>
                <Body>
                    <Title>Add Top</Title>
                </Body>
                <Right />
            </Header>
            <ScrollView style={style.imgMask} >
            <View style={{ flex: 1, flexDirection:'row' }}>
                <View style={{ flex: 1, backgroundColor: '#635DB7', height: 200 }}>
                    <KeyboardAvoidingView behavior="position">
                    <Field label="Name"  autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="default"
                                returnKeyType="next"
                                value={this.state.top_name}
                                onChangeText={(top_name) => this.setState({top_name})}
/>
                    </KeyboardAvoidingView>
                    
                    <Col style={{ justifyContent: 'center', alignItems: 'center' }}>  
                       
                        <TouchableOpacity onPress={this.getFirstImage}>
                         {this.state.image_file!=''&&<Image source={{uri:this.state.image_file}} style={{width:100,height:100, justifyContent: 'center', alignItems: 'center'}}/>}
                        {this.state.image_file==''&&<Avatar size={100} style={{ justifyContent: 'center', alignItems: 'center'}}/>}

                         </TouchableOpacity>
                        
                    </Col>
                </View>

            </View>



  
            </ScrollView>


            <Button primary full style={{ height: variables.footerHeight }} onPress={this.addTop}>
               {this.state.isLoading ? <Spinner color="white"/> : <Text style={{ color:'#fff' }}>SAVE</Text>} 
            </Button>
            <Toast ref="toast"/> 
        </Container></Image>;
    }
}

const style = StyleSheet.create({
 heading: {
        marginTop: variables.contentPadding * 2,
        color: "white"
    },
      date:{
    flexDirection: 'row',
    height:550
  },
  rating:{
    textAlign:'center', 
    margin:12
  },
ratinginner:{
    alignItems: 'center', 
    justifyContent: 'center',
    width:60, 
    height:60, 
    textAlign:'center',
    padding:20,
    borderRadius: 100
  },
  ratingtext:{
    color:'#fff',
    fontSize:16
  },
          img: {
        resizeMode: "cover",
        ...WindowDimensions
    },
            imgMask: {
        backgroundColor: "rgba(71, 255, 247, .4)"
    },

});