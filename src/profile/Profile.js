// @flow
import autobind from "autobind-decorator";
import React, {Component} from "react";
import {View, Image, StyleSheet, KeyboardAvoidingView, ScrollView ,Text,CameraRoll,TouchableOpacity,ActivityIndicator} from "react-native";
import {H1} from "native-base";
import {Container, Button, Header, Left, Right, Body, Icon, Title,Spinner} from "native-base";
import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";

import {BaseContainer, Avatar, TaskOverview,Images, Small, Styles, Task, Field,WindowDimensions, NavigationHelpers} from "../components";

import variables from "../../native-base-theme/variables/commonColor";

import api from '../Utils/api';
import helper from '../Utils/helper';

import Toast,{DURATION} from "react-native-easy-toast";

import { ImagePicker } from 'expo';

export default class Profile extends Component {

    constructor(props) {
        super(props)

        this.image_path = 'http://voxapp.studiowebdemo.com/services/images/user_image/';
        this.imagechange= false;
        this.state = {
            isLoading: false,
            containerloading:true,
            user_name:'',
            user_email:'',
            user_password:'',
            user_phonenumber:'',
            user_age:'',
            user_country:'',
            city_town:'',
            user_repeatpassword:'',
            photo:'',
            disabled:true

        }

       
  }

     componentWillMount() {

        helper.getCache('@userdata').then((res) => {

            console.log(res);
            if(res){

                this.userData = JSON.parse(res);
                let profileImage = '';
                if(this.userData.photo!=''){

                    profileImage = this.image_path+this.userData.photo;
                }
                console.log(profileImage);

                this.setState({

                    isLoading: false,
                    containerloading:false,
                    photo:profileImage,
                    user_name:this.userData.user_name,
                    user_email:this.userData.user_email,
                    user_password:this.userData.user_password,
                    user_phonenumber:this.userData.user_phonenumber,
                    user_age:this.userData.user_age,
                    user_country:this.userData.user_country,
                    city_town:this.userData.city_town
                })
                
            }else{

                this.setState({loading:false}); 
            }
            
           
        })

        
    //const userdata = await AsyncStorage.getItem('@userdata'); 
    }

    props: {
        navigation: NavigationScreenProp<*, *>
    }

    @autobind
    back() {
        this.props.navigation.goBack();
    }

    @autobind
    updateProfile() {
        try {
            
             var user_email = this.state.user_email;
             var user_name = this.state.user_name;
             var user_password = this.state.user_password;
             var user_phonenumber = this.state.user_phonenumber;
             var user_age = this.state.user_age;
             var user_country = this.state.user_country;
             var city_town = this.state.city_town;
             var user_repeatpassword = this.state.user_repeatpassword;

             var emailRegex = /^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$/;

             if(user_name==''){

                this.refs.toast.show('Please provide username');

             }else if(user_email==''){

                this.refs.toast.show('Please provide email address');

             }else if(!emailRegex.test(user_email)){

                this.refs.toast.show('Email format is invalid');

             }else{

                this.setState({isLoading:true});
                
                let data;

                if(this.imagechange){

                    let imagedata ={
                        uri:this.state.photo,
                        name:'profile.jpg',
                        type:'image/jpg'
                    }

                    data ={
                        user_id:this.userData.user_id,
                        user_name:user_name,
                        user_email:user_email,
                        user_phonenumber:user_phonenumber,
                        user_age:user_age,
                        user_country:user_country,
                        city_town:city_town,
                        image_file:imagedata
                    }
                }else{

                    data ={
                        user_id:this.userData.user_id,
                        user_name:user_name,
                        user_email:user_email,
                        user_phonenumber:user_phonenumber,
                        user_age:user_age,
                        user_country:user_country,
                        city_town:city_town
                    }
                }
                

                //console.log(data);
                api.postApi('user_update_service.php',data)
                  .then((res) => {

                    console.log(res);
                    this.setState({isLoading:false,disabled:true});
                    if(res.Result=='True'){

                        if(this.imagechange){

                            
                            let imageLength = res.user_image.split('/');
                            this.userData.photo = imageLength[imageLength.length-1];
                            this.imagechange= false;
                        }
                        

                        this.userData.user_name = user_name;
                        this.userData.user_email = user_email;
                        this.userData.user_phonenumber = user_phonenumber;
                        this.userData.user_age = user_age;
                        this.userData.user_country = user_country;
                        this.userData.city_town = city_town;

                        helper.setCache('@userdata',this.userData);
                        this.refs.toast.show(res.ResponseMsg);

                    }else{

                        this.refs.toast.show(res.ResponseMsg);
                    }

                    
                    
                  })
                  .catch((e) => {
                    // alert(e.message);
                     this.refs.toast.show(e.message);
                     this.setState({isLoading:false});
                  })


             }

             

        } catch(e) {
            this.setState({isLoading:false});
            this.refs.toast.show(e.message);
        } 
    }


    @autobind
    async getImage(): Promise<void> {
        console.log('camera start');
        // CameraRoll.getPhotos({
        //     first:20
        // }).then(r=>{
        //     console.log(r.edges);
        // }).catch((err)=>{

        //     console.log('err images===',err);
        // })

        let result = await ImagePicker.launchCameraAsync({
            allowEditing:true,
            aspect:[4,3]
        })

        //console.log(result);

        if(!result.cancelled){

            console.log(result.uri);
            this.imagechange =true;
            this.setState({photo:result.uri,disabled:false});
        }
    }

    @autobind
    signIn() {
        NavigationHelpers.reset(this.props.navigation, "Walkthrough");
    }

    

    changeState = (type) => {

      
        this.setState(type);
        this.setState({disabled:false})
    }

    render(): React$Element<*> {

        if (this.state.containerloading) {

            return <Image source={Images.login} style={style.img} ><Container>
            <Toast ref="toast"/> 
            <Header noShadow>
                <Left>
                    <Button onPress={this.back} transparent>
                        <Icon name='close' />
                    </Button>
                </Left>
                <Body>
                    <Title>Profile</Title>
                </Body>
                <Right />
            </Header>
            {
                    <View style={ Styles.flex }>
                      <ActivityIndicator
                        animating={ this.state.containerloading }
                        style={[
                          Styles.centering,
                          { height: 80 }
                        ]} size="large" />
                    </View>
                }
                </Container></Image>;
            

        }else{

            return <Image source={Images.login} style={style.img} ><Container>
            <Toast ref="toast"/> 
            <Header noShadow>
                <Left>
                    <Button onPress={this.back} transparent>
                        <Icon name='close' />
                    </Button>
                </Left>
                <Body>
                    <Title>Profile</Title>
                </Body>
                <Right />
            </Header>
            <ScrollView style={style.imgMask} >
                                <View style={[Styles.header, Styles.center]}>
                        <H1  style={{ marginTop: variables.contentPadding * 2 }}>{this.state.user_name}</H1>
                        
                        <TouchableOpacity onPress={this.getImage}>
                         {this.state.photo!=''&&<Image style={{width:100,height:100}} source={{uri:this.state.photo}}/>}
                        {this.state.photo==''&&<Avatar size={100}/>}
                        </TouchableOpacity>
                     </View>
                <KeyboardAvoidingView behavior="padding">

                    <Field label="Username" autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="default"
                                returnKeyType="next"
                                value={this.state.user_name}
                                onChangeText={(user_name) => this.changeState({user_name})}
                                 />
                    <Field label="Email"
                                autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="email-address"
                                returnKeyType="next"
                                value={this.state.user_email}
                                editable={false}
                                selectTextOnFocus={false}
                                
                                 />
                    <Field label="Phone Number" 
                                autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="phone-pad"
                                returnKeyType="next"
                                value={this.state.user_phonenumber}
                                onChangeText={(user_phonenumber) => this.changeState({user_phonenumber})}
                                />
                    <Field label="Age" 
                                autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="phone-pad"
                                returnKeyType="next"
                                value={this.state.user_age}
                                onChangeText={(user_age) => this.changeState({user_age})}
                                />
                    <Field label="Country" 
                                autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="default"
                                returnKeyType="next"
                                value={this.state.user_country}
                                onChangeText={(user_country) => this.changeState({user_country})}
                                />
                    <Field label="City/Town" 
                                autoCapitalize="none"
                                autoCorrect={false}tger
                                keyboardType="default"
                                returnKeyType="next"
                                value={this.state.city_town}
                                onChangeText={(city_town) =>this.changeState({city_town})}
                                />
                </KeyboardAvoidingView>
            </ScrollView>
            <Button disabled={this.state.disabled} primary full onPress={this.updateProfile} style={{ height: variables.footerHeight }} >
               {this.state.isLoading ? <Spinner color="white"/> : <Text style={{ color:'#fff' }}>SAVE</Text>} 
            </Button>
        </Container></Image>;
        }
        
    }
}

const style = StyleSheet.create({
    circle: {
        backgroundColor: "white",
        height: 125,
        width: 125,
        borderRadius: 62.5,
        justifyContent: "center",
        alignItems: "center"
    },
            img: {
        resizeMode: "cover",
        ...WindowDimensions
    },
            imgMask: {
        backgroundColor: "rgba(71, 255, 247, .4)"
    },
});