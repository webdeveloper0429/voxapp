// @flow
import autobind from "autobind-decorator";
import React, {Component} from "react";
import {View, Image, StyleSheet, KeyboardAvoidingView, ScrollView ,Text, Picker} from "react-native";
import {H1} from "native-base";
import {Container, Button, Header, Left, Right, Body, Icon, Title} from "native-base";
import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";
import { EvilIcons } from "@expo/vector-icons";
import { Col, Row, Grid } from 'react-native-easy-grid';
import {BaseContainer, Avatar, TaskOverview, Small, Styles, Task, Field, NavigationHelpers, Images, WindowDimensions} from "../components";

import variables from "../../native-base-theme/variables/commonColor";
import api from '../Utils/api';
import helper from '../Utils/helper';

import Toast,{DURATION} from "react-native-easy-toast";

export default class Adddualprofile extends Component {

    constructor(props) {
        
        super(props)

        let param = this.props.navigation.state.params.data;
        console.log('===param==param',param);
        this.state = {
          loading: true,
          duelList:'',
          firstname:param.firstname,
          secondname:param.secondname,
          firstvotes:param.firstvotes,
          secondvotes:param.secondvotes,
          firstphoto:param.firstphoto,
          secondphoto:param.secondphoto,
          duel_id:param.duel_id
        }

       
    }

    props: {
        navigation: NavigationScreenProp<*, *>
    }

    componentWillMount() {

        helper.getCache('@userdata').then((res) => {

            console.log(res);
            if(res){

                this.userData = JSON.parse(res);

                this.setState({token:this.userData.tokens}); 
                
            }else{

                this.setState({loading:false}); 
            }
            
           
        })

        
    //const userdata = await AsyncStorage.getItem('@userdata'); 
    }

    @autobind
    back() {
        this.props.navigation.goBack();
    }

    @autobind
    signIn() {
        NavigationHelpers.reset(this.props.navigation, "Walkthrough");
    }

    voteAction(type){

            

            let data ={
                user_id:this.userData.user_id,
                is_first_second:type
            }

            this.setState({loading:true})

            console.log('data==',data);

            api.postApi('add_duel_vote_service.php',data)
                  .then((res) => {

                    console.log(res);
                    if(res.Result=='True'){
                       
                        this.setState({loading:false}); 
                        this.refs.toast.show(res.ResponseMsg);

                    }else{

                          this.refs.toast.show(res.ResponseMsg);
                         this.setState({loading:false}); 
                    }
                   
            })
            .catch((e) => {

                     //alert(e.message);
                this.refs.toast.show(e.message);
                this.setState({loading:false});

            })

    }

    render(): React$Element<*> {

        const {firstname, secondname,firstvotes,secondvotes,firstphoto,secondphoto,token} = this.state;
        return <Container>
        <Image source={Images.login} style={style.img} >
            <Header noShadow>
                <Left>
                    <Button onPress={this.back} transparent>
                        <Icon name='close' />
                    </Button>
                </Left>
                <Body>
                    <Title>View Dual Profile</Title>
                </Body>
                <Right />
            </Header>
            <ScrollView style={style.imgMask} >
               <Grid>
              <Col style={{ backgroundColor: '#635DB7', height: 200,justifyContent: 'center',
                  alignItems: 'center'}}>
                <View style={{ flex:1, flexDirection:'row', height: 20}}>
                  
                </View>
                {firstphoto!=''&&<Image style={{width:120,height:120}} source={{uri:firstphoto}}/>}
                {firstphoto==''&&<Avatar size={120}/>}
                <Text style={{ color:'#fff', fontSize:18, padding:10}}>Name : {firstname}</Text>
                
              </Col>
              <Col style={{ backgroundColor: '#00CE9F', height: 200, justifyContent: 'center',
                  alignItems: 'center'}}>
                  <View style={{ flex:1, flexDirection:'row', height: 20}}>
                  
                </View>
                 {secondphoto!=''&&<Image style={{width:120,height:120}} source={{uri:secondphoto}}/>}
                {secondphoto==''&&<Avatar size={120}/>}
                <Text style={{ color:'#fff', fontSize:18, padding:10}}>Name : {secondname}</Text>
                
              </Col>
            </Grid>

            <Grid style={{ height: 150}}>
                <Col style={{ justifyContent: 'center',alignItems: 'center' }}>  
                    <Text style={{marginTop:10, width:70, height:70, borderRadius:100, backgroundColor: '#635DB7', color: '#fff', fontSize:20, paddingTop:20 , fontWeight:'bold', textAlign:'center'}} onPress={() => this.voteAction('F')} >VP</Text>
                </Col>
                <Col style={{ justifyContent: 'center',alignItems: 'center' }}>
                    <Text style={{marginTop:10, width:70, height:70, borderRadius:100, backgroundColor: '#00CE9F', color: '#fff',fontSize:20, paddingTop:20 , fontWeight:'bold', textAlign:'center'}} onPress={() => this.voteAction('S')} >VP</Text>
                </Col>
              </Grid>
              <Grid>
                <Col style={{ height: 20, justifyContent: 'center',
                    alignItems: 'center' }}>  
                    <Text style={{color: 'red', fontSize:30, fontWeight:'bold'}}>Points</Text>
                </Col>
              </Grid>
              <Grid>
                <Col style={{ justifyContent: 'center',alignItems: 'center' }}>  
                    <Text style={{marginTop:10, width:70, height:70,  color: 'red', fontSize:20, paddingTop:20 , fontWeight:'bold', textAlign:'center'}}>{firstvotes}</Text>
                </Col>
                <Col style={{ justifyContent: 'center',alignItems: 'center' }}>
                    <Text style={{marginTop:10, width:70, height:70, color: 'red',fontSize:20, paddingTop:20 , fontWeight:'bold', textAlign:'center'}}>{secondvotes}</Text>
                </Col>
              </Grid>
              <Grid>
                <Col >  
                    <Text style={{color: 'red', fontSize:25, fontWeight:'bold', padding:15}}>VP : {token}</Text>
                </Col>
              </Grid>
                  
            </ScrollView>
            <Toast ref="toast"/>
            </Image> 
        </Container>;
    }
}

const style = StyleSheet.create({
    circle: {
        backgroundColor: "white",
        height: 125,
        width: 125,
        borderRadius: 62.5,
        justifyContent: "center",
        alignItems: "center"
    },
        img: {
        resizeMode: "cover",
        ...WindowDimensions
    },
            imgMask: {
        backgroundColor: "rgba(71, 255, 247, .4)"
    },
});