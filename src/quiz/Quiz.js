// @flow
import moment from "moment";
import autobind from "autobind-decorator";
import React, {Component} from "react";
import {StyleSheet, Image, View, Text, ScrollView, Dimensions,ActivityIndicator} from "react-native";
import {H1,Container, Button, Header, Left, Right, Body, Icon, Card, CardItem, Title, ListItem, CheckBox,Spinner} from "native-base";

import {BaseContainer, Styles, Images, Avatar, Task, WindowDimensions} from "../components";
import { Col, Row, Grid } from 'react-native-easy-grid';
import variables from "../../native-base-theme/variables/commonColor";

import Toast,{DURATION} from "react-native-easy-toast";


import api from '../Utils/api';
import helper from '../Utils/helper';

export default class Quiz extends Component {
    

     constructor(props) {
    super(props)

        this.state = {
          loading: false,
          question:this.props.navigation.state.params.question,
          quiz_id:this.props.navigation.state.params.quiz_id,
          answer_a:this.props.navigation.state.params.answer_a,
          answer_b:this.props.navigation.state.params.answer_b,
          answer_c:this.props.navigation.state.params.answer_c,
          answer_d:this.props.navigation.state.params.answer_d,
          userid:this.props.navigation.state.params.userid,
          isCheckA:false,
          isCheckB:false,
          isCheckC:false,
          isCheckD:false

        }

       console.log('params====',this.props.navigation.state.params);
    }

    @autobind
    back() {
        this.props.navigation.goBack();
    }

    checkAnwser = (anwser) => {
  
        this.setState({isCheckA:false,isCheckB:false,isCheckC:false,isCheckD:false})
        if(anwser=='a'){

          this.setState({isCheckA:true})
        }else if(anwser=='b'){

          this.setState({isCheckB:true})
        }else if(anwser=='c'){

          this.setState({isCheckC:true})
        }else if(anwser=='d'){

          this.setState({isCheckD:true})
        }
        
        
    }

    updateData(){

            let data ={
                user_id:this.state.userid,
                quiz_id:this.state.quiz_id
            }

            this.setState({loading:true})

            api.postApi('add_quiz_answer_service.php',data)
                  .then((res) => {

                    console.log(res);
                    if(res.Result=='True'){
                       
                        this.setState({loading:false}); 
                        this.refs.toast.show(res.ResponseMsg);

                    }else{

                          this.refs.toast.show(res.ResponseMsg);
                         this.setState({loading:false}); 
                    }
                   
            })
            .catch((e) => {

                     //alert(e.message);
                //this.refs.toast.show(e.message);
                this.setState({loading:false});

            })
    }

   


    render(): React$Element<*> {
        const screenHeight = Dimensions.get('window').height

        return <Image source={Images.login} style={style.img} >
        <Container style={style.imgMask}>
             
            <Header noShadow>
                <Left>
                    <Button onPress={this.back} transparent>
                        <Icon name='close' />
                    </Button>
                </Left>
                <Body>
                    <Title>Quiz</Title>
                </Body>
                <Right />
            </Header>
                <Card style={{backgroundColor: "rgba(71, 255, 247, .4)"}}>
                    <CardItem>
                      <Body>
                        <Text style={style.question}>
                          {this.state.question}
                        </Text>
                      </Body>
                    </CardItem>

                      <ListItem >
                        <CheckBox checked={this.state.isCheckA}  onPress={() => this.checkAnwser('a')}/>
                        <Body>
                          <Text style={style.answer}>{this.state.answer_a}</Text>
                        </Body>
                      </ListItem>
                      <ListItem>
                        <CheckBox checked={this.state.isCheckB}  onPress={() => this.checkAnwser('b')}/>
                        <Body>
                          <Text style={style.answer}>{this.state.answer_b}</Text>
                        </Body>
                      </ListItem>
                      <ListItem >
                        <CheckBox checked={this.state.isCheckC} onPress={() => this.checkAnwser('c')}/>
                        <Body>
                          <Text style={style.answer}>{this.state.answer_c}</Text>
                        </Body>
                      </ListItem>
                      <ListItem >
                        <CheckBox checked={this.state.isCheckD} onPress={() => this.checkAnwser('d')}/>
                        <Body>
                          <Text style={style.answer}>{this.state.answer_d}</Text>
                        </Body>
                      </ListItem>
                 </Card>

                 
                    <Button primary full onPress={() => this.updateData()}>
                                    {this.state.loading ? <Spinner color="white" /> : <Text>Update</Text>}
                    </Button>
                   
                   <Toast ref="toast"/> 
            </Container></Image>;
        
    }
}

const style = StyleSheet.create({
    heading: {
        marginTop: variables.contentPadding * 2,
        color: "white"
    },
    question:{
      fontSize:20,
      fontWeight:'bold'
    },
    answer:{
        paddingLeft:10
    },
            img: {
        resizeMode: "cover",
        ...WindowDimensions
    },
            imgMask: {
        backgroundColor: "rgba(71, 255, 247, .4)"
    },
});